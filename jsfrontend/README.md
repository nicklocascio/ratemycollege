# User Interaction

1) User can search for a school on the homepage to see the basic information, a photo, and a link to the school's website
2) If the school does not exist, user can click on the link to add the school and they are prompted for all of the necessary information for the school before submitting
3) User can navigate to the review page to either add a review for their school or to read reviews for schools by searching the school
4) User can navigate to the explore page and click on one of the three datasets to explore. Data is first presented in table form but the user can click on the visualize button and that data will be converted into a graph, where they can hover over each bar to see the school name and the associated data

# Videos

Demo - https://drive.google.com/file/d/1QYVO6t8Mh25tumQhWk2cQWl4dywQZR0L/view?resourcekey

Code - https://www.youtube.com/watch?v=rqfJ1hJK0XI

# Slides

https://docs.google.com/presentation/d/1o276XqZsx6GW6gQl-Z3PdbqRVSQEurjGDiflbSKciIo/edit#slide=id.p

# Complexity

One source of complexity in our project was visualizing the data on a graph on the explore page. We were working with a third party library that contained several tricky nuances that we had to work around in order to make the explore page usable and clean. It was tricky to figure out how to organize the execution of the necessary functions and how to handle the scope of different variables and data. 

Another area of complexity in the project came from the organization of our data to make it scalable and meaningful to the user. We wanted our data source to be easy to work with so that the controller/library didn’t have any unnecessary functions, but we also wanted to make sure that it was built for many users to add review and ratings
