console.log('loaded js');

var submit = document.getElementById('submit-form');
submit.onclick = getFormInfo;

function getFormInfo() {
    console.log('in form info');
    var name = document.getElementById('school-name').value;
    var enrollment = document.getElementById('enrollment').value;
    var grad_rate = document.getElementById('grad').value;
    var price = document.getElementById('price').value;
    var url;
    try {
        url = document.getElementById('url').value;
    } catch(err) {
        url = "";
    }
    var image;
    try {
        image = document.getElementById('image').value;
    } catch(err) {
        image = "";
    }

    if(name == "" || enrollment == "" || grad_rate == "" || price == "") {
        return;
    } else {
        addSchool(name, enrollment, grad_rate, price, url, image);
    }
}

function addSchool(name, enrollment, grad_rate, price, url, image) {
    console.log('in add school');

    // set up POST request
    var request_url = "http://localhost:51045/schools/"
    var xhr = new XMLHttpRequest();
    xhr.open("POST", request_url, true);

    // set up message body
    var body = {
        "Name": name,
        "Enrollment": parseInt(enrollment),
        "Graduation Rate": (grad_rate + '%'),
        "Price": ('$' + price),
        "Url": url,
        "Image": image
    };
    console.log(body);

    xhr.onload = function(e) {
        console.log(xhr.responseText);
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(JSON.stringify(body));
}
