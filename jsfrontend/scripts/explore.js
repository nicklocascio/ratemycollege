// code for explore.js
console.log('loaded js');

// lists for data
var schools_arr = [];
var data_arr = [];
var type = "";

config = {
    type: 'bar',
    data: {},
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    },
};
var chart = new Chart(
    document.getElementById('chart'),
    config
);
chart.clear();

// set up event listeners
var enrollment_click = document.getElementById("enrollment-click");
enrollment_click.addEventListener("click", getEnrollment);

var grad_click = document.getElementById("grad-click");
grad_click.addEventListener("click", getGrad);

var price_click = document.getElementById("price-click");
price_click.addEventListener("click", getPrice);

var visualize_button = document.getElementById("visualize-button");
visualize_button.addEventListener("click", updateChart);

// call getData with specified data type
function getEnrollment() {
    console.log("e");
    type = "Enrollment";
    getData(type);
}
function getGrad() {
    console.log("g");
    type = "Graduation Rate";
    getData(type);
}
function getPrice() {
    console.log("p");
    type = "Price";
    getData(type);
}

// make call to API
function getData(type) {
    console.log('in get data ' + type);
    var url = "http://localhost:51045/schools/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        //console.log(xhr.responseText);
        updateDisplay(xhr.responseText, type);
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(null);
}

// display the data based on what the user clicked
function updateDisplay(response_text, type) {
    var response_json = JSON.parse(response_text);
    var i = 0;
    var updating = true;
    var data_div = document.getElementById('data');
    data_div.innerHTML = "";
    var table = document.createElement('table');
    table.className = 'table table-bordered'
    var tr = document.createElement('tr');
    var th1 = document.createElement('th');
    var th2 = document.createElement('th');
    var head1 = document.createTextNode("School");
    var head2 = document.createTextNode(type);
    th1.appendChild(head1);
    th2.appendChild(head2);
    tr.appendChild(th1);
    tr.appendChild(th2);
    table.appendChild(tr);
    schools_arr = [];
    data_arr = [];
    chart.clear();
    while(updating) {
        try {
            var name = response_json[i.toString()]["Name"];
            var data = "";
            var data_int = 0;
            if(type == "Enrollment") {
                data = response_json[i.toString()]["Enrollment"];
                data_int = data;
            } else if(type == "Graduation Rate") {
                data = response_json[i.toString()]["Graduation Rate"];
                data_int = parseInt(data.replace('%', ''));
                console.log(data_int);
            } else {
                data = response_json[i.toString()]["Price"];
                data_int = parseInt(data.replace(/[$,]/g, ''));
                console.log(data_int);
            }
            //console.log(name + ': ' + data);
            schools_arr.push(name);
            data_arr.push(data_int);
            var tr = document.createElement('tr');
            var td1 = document.createElement('td');
            var td2 = document.createElement('td');
            var text1 = document.createTextNode(name);
            var text2 = document.createTextNode(data);
            td1.appendChild(text1);
            td2.appendChild(text2);
            tr.appendChild(td1);
            tr.appendChild(td2);
            table.appendChild(tr);
            //data_element.innerHTML += ('<br><h3>' + name + ': ' + data + '</h3>');
        } catch(err) {
            console.log(err);
            updating = false;
        }
        i += 1;
    }
    data_div.appendChild(table);
}

function removeData(chart) {
    console.log(chart.data.labels);
    chart.data.labels = [];
    chart.data.datasets.pop();
    chart.update();
}

function addData(chart, labels, data) {
    console.log('in add');
    console.log(chart);
    labels.forEach(element => {
        chart.data.labels.push(element);
    })
    chart.data.datasets.push(data);
    chart.update();
}

function updateChart() {
    console.log('in update');
    var data_div = document.getElementById('data');
    data_div.innerHTML = "";
    removeData(chart);
    data_obj = {
        label: type,
        data: data_arr,
        backgroundColor: [
          /*'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',*/
          'rgba(58, 193, 98, 0.2)',
          /*'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)'*/
        ],
        borderColor: [
          /*'rgb(255, 99, 132)',
          'rgb(255, 159, 64)',
          'rgb(255, 205, 86)',
          'rgb(75, 192, 192)',*/
          'rgb(58, 193, 98)',
          /*'rgb(153, 102, 255)',
          'rgb(201, 203, 207)'*/
        ],
        borderWidth: 1
    };
    addData(chart, schools_arr, data_obj);
}
