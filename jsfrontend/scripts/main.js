// code for home page
console.log('loaded js');

function populateSearch() {
    console.log("in here");
    var url = "http://localhost:51045/schools/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function(e) {
        var json_resp = JSON.parse(xhr.responseText);
        var datalist = document.getElementById("schools-datalist");
        for(let entry in json_resp) {
            var name = json_resp[entry]["Name"]
            if(name != undefined) {
                console.log(name);
                var option = document.createElement('option');
                option.setAttribute('value', name);
                datalist.appendChild(option);
            }
        }
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(null);
}

var submit_click = document.getElementById("search-submit");
submit_click.addEventListener("click", getSchool);

function getSchool(){
    console.log("gs");
    var school_name = document.getElementById("search-school").value;
    var url = "http://localhost:51045/schools/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function(e) {
        getSchoolData(xhr.responseText, school_name);
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(null);
}

function getSchoolData(response_text, name){
    var response_json = JSON.parse(response_text);
    var header = document.getElementById("search-result-header");
    var data = document.getElementById("search-data");
    var image = document.getElementById("search-school-image");
    var link = document.getElementById("search-link");
    header.innerHTML = "";
    data.innerHTML = "";
    image.src = "";
    link.innerHTML = "";
    link.href = "";
    image.src = "";
    var id = -1;
    for(let entry in response_json){
        if(entry == 'result'){
            continue;
        }else if(response_json[entry]['Name'] == name){
            console.log(response_json[entry]);
            id = entry;
            break;
        }
    }
    if(id < 0){
        // TODO link page to enter new school
        link.href = "add_school.html";
        link.innerHTML = "<h4>Can't find your school? Add it here!</h4>"
        image.src = "";
    }else{
        if(response_json[id]["Ratings"].length > 0){
            var avg = 0;
            var count = 0;
            for(i = 0; i < response_json[id]["Ratings"].length; i++){
                avg += response_json[id]["Ratings"][i];
                count += 1;
            }
            var display_name = response_json[id]["Name"] + " (" + Math.round((avg/count)*10)/10 + " out of 5 stars)";
        }else{
            var display_name = response_json[id]["Name"];
        }
        header.innerHTML = "Search Results:";
        data.innerHTML = "<h2>" + display_name + ":</h2>" + "<br><h4>Enrollment: " + response_json[id]["Enrollment"] + "</h4><br><h4>Graduation Rate: " + response_json[id]["Graduation Rate"] + "</h4><br><h4>Price: " + response_json[id]["Price"] + "</h4>";
        image.src = response_json[id]["Image"];
        image.width = "1200";
        image.height = "700";
        link.href = response_json[id]["Url"];
        link.innerHTML = "<h4>Learn more!<br></h4>";
    }

}
