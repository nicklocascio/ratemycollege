// code for review page
console.log("loaded review js");

function populateSearch() {
    console.log("in here");
    var url = "http://localhost:51045/schools/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function(e) {
        var json_resp = JSON.parse(xhr.responseText);
        var datalist = document.getElementById("schools-datalist");
        for(let entry in json_resp) {
            var name = json_resp[entry]["Name"]
            if(name != undefined) {
                console.log(name);
                var option = document.createElement('option');
                option.setAttribute('value', name);
                datalist.appendChild(option);
            }
        }
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(null);
}

var submit_click = document.getElementById("submit-review-click");
submit_click.addEventListener("click", putReview);

var search_click = document.getElementById("search-submit");
search_click.addEventListener("click", getReviews);

function putReview(){
    console.log("pr");
    document.getElementById("send-success").innerHTML = "";
    var reviewer_name = document.getElementById("reviewer-name").value;
    var school_name = document.getElementById("school-name").value;
    var school_review = document.getElementById("school-review").value;
    var rating = parseInt(document.querySelector('input[name="stars"]:checked').value);
    console.log("Rate: " + rating);
    getID(school_name, reviewer_name, school_review, rating);
}

function getReviews(){
    console.log("gr");
    var school_name = document.getElementById("search-school").value;
    getData(school_name);
}

function getAverage(values){
    var sum = 0;
    var len = 0;
    for(let val in values){
        sum += values[val];
        len += 1;
    }


    return sum / len;
}

function getData(name){
    console.log("in get data");
    var url = "http://localhost:51045/schools/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    //var response_msg = document.getElementById("send-success");
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        //response_msg.innerHTML = "Review submitted successfully";
        displayData(xhr.responseText, name);
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(null);

}

function displayData(response_text, name){
    var response_json = JSON.parse(response_text);
    var id = -1;
    var ratings = null;
    var data = document.getElementById("reviews");
    data.innerHTML = "";
    var header = document.getElementById("reviews-header");
    var rate_html = document.getElementById("ratings");
    for(let entry in response_json){
        if(entry == 'result'){
            continue;
        }else if(response_json[entry]['Name'] == name){
            id = entry;
            break;
        }
    }
    ratings = response_json[id]['Ratings']
    if(ratings.length){
        rate_html.innerHTML = "Average Rating: " + (getAverage(ratings)).toFixed(1) + " out of 5 stars";
    }else{
        rate_html.innerHTML = "No ratings have been submitted for this school.<br>Please submit one above if you'd like.";
    }
    if(Object.keys(response_json[id]['Reviews']).length){
        for(let key in response_json[id]['Reviews']){
            data.innerHTML += ('"' + response_json[id]['Reviews'][key] +'" - ' + key + '<br><br>');
            header.innerHTML = "<br>Reviews:";
        }
    }else{
        data.innerHTML = "No reviews have been submitted for this school.<br>Please submit one above if you'd like.";
    }


}

function putData(response_text, school, name, review, rating){
    console.log('in put review');
    var response_json = JSON.parse(response_text);
    var id = -1;
    for(let entry in response_json){
        if(entry == 'result'){
            continue;
        }else if(response_json[entry]['Name'] == school){
            id = entry;
        }
    }
    console.log('ID: ' + id);
    if(!review | !rating){
        return;
    }

    var url = "http://localhost:51045/schools/" + id + "/review";
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", url, true);
    var response_msg = document.getElementById("send-success");
    var body = {'Username': name, 'Review': review, 'Rating': rating};
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        response_msg.innerHTML = "Review submitted successfully";
        //putData(xhr.responseText, school, name, review);
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(JSON.stringify(body));

}

function getID(school, name, review, rating){
    console.log('in get ID');
    var url = "http://localhost:51045/schools/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        console.log(xhr.responseText);
        putData(xhr.responseText, school, name, review, rating);
    }
    xhr.onerror = function(e) {
        console.log(xhr.statusText);
    }

    xhr.send(null);
}
