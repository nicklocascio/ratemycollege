# REST API specification

| Request<br>Type | Resource<br>Endpoint |                                                                               Body                                                                              |                          Expected<br>Response                          |                        Inner Workings<br>of Handler                       |
|:---------------:|:--------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------:|:-------------------------------------------------------------------------:|
|       GET       | /schools/            | N/A                                                                                                                                                             | string formatted JSON of <br>all schools and their <br>associated data | GET_ALL<br>goes through all <br>schools and returns <br>their information |
|       GET       | /schools/:id         | N/A                                                                                                                                                             | string formatted JSON of <br>just one school’s <br>information         | GET_KEY<br>returns just a <br>given school’s <br>data                     |
|       PUT       | /schools/:id         | {"Name: "Notre Dame"} or<br>{"Enrollment": 8000} or<br>{"Graduation Rate": "98%"} or<br>{"Price": "$70000"} or<br>{"Url": "nd.edu"} or<br>{"Image": "nd.jpg"}   | {“result”: “success”} <br>if the operation worked                      | PUT_KEY<br>updates a school <br>entry based on <br>the body               |
|       PUT       | /schools/:id/review  | {“Username”: “username”, <br>“Review”: “reviewText”, <br>“Rating”: 4}                                                                                           | {“result”: “success”} <br>if the operation worked                      | PUT_REVIEW<br>adds a new review <br>for a school                          |
|       POST      | /schools/            | {“Name”: “Indiana University”, <br>“Enrollment”: 25,000, <br>“Graduation Rate”: “91%”, <br>“Price”, “$20,000”, <br>“Url”: “iu.edu”, <br>“Image”: “linkToImage”} | {“result”: “success”,<br>"id": "newID"} <br>if the operation worked    | POST<br>creates a new school <br>entry from the data <br>in body          |
|      DELETE     | /schools/            | {}                                                                                                                                                              | {“result”: “success”} <br>if the operation worked                      | DELETE_KEY<br>deletes a specific <br>school based on name                 |
|      DELETE     | /schools/:id         | {}                                                                                                                                                              | {“result”: “success”} <br>if the operation worked                      | DELETE_ALL<br>deletes all schools <br>in the database                     |

# OO API

load_schools(file) - this will load all of the schools into a dictionary which will be used as the schools database

update_json() - whenever we have a PUT or POST request, we call this function to update the json file of data so that any changes will persist across sessions and for all users

get_schools() - this function returns the keys of all of the schools that we can then use to get individual school information

get_school(id) - this function will return all of the information for a given school, for the GET request to get all schools, we first get they keys from get_schools and then for each key we call get_school(key) to get the data for each school

set_school(id, data) - this is what we use for the PUT requests to update a specific field for a school. Given a school id, we update the field that matches the key in data with the value in data

delete_school(id) - this simply deletes a school from the dictionary, we are not planning on giving users access to this function (as well as the DELETE_ALL and DELETE_KEY API functions), but kept it in here for administrative purposes
