import cherrypy
import json
from schools_library import _schools_database

class ResetController(object):
    def __init__(self, school_db=None):
        if school_db is None:
            self.school_db = _schools_database()
        else:
            self.school_db = school_db

    def PUT_INDEX(self):
        output = {'result': 'success'}

        data = json.loads(cherrypy.request.body.read().decode())

        self.school_db.__init__()
        self.school_db.load_schools('colleges.json')

        return json.dumps(output)
