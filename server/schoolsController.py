import cherrypy
import json
from schools_library import _schools_database

class SchoolController(object):
    def __init__(self, school_db=None):
        if school_db is None:
            self.school_db = _schools_database()
        else:
            self.school_db = school_db

        # load the data into school_db, a _schools_database object
        self.school_db.load_schools('colleges.json')

    def GET(self):
        output = {'result': 'success'}
        schools = list(self.school_db.get_schools())
        for id in schools:
            output[id] = self.school_db.get_school(id)
        return json.dumps(output)


    def GET_KEY(self, id):
        output = {'result': 'success'}

        try:
            school = self.school_db.get_school(str(id))
            output['school'] = school
        except Exception as ex:
            output['result']: 'error'
            output['message']: str(ex)

        return json.dumps(output)

    def PUT_KEY(self, id):
        output = {'result': 'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))
        try:
            suc = self.school_db.set_school(id, data)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)

    def PUT_REVIEW(self, id):
        output = {'result': 'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))
        try:
            school = self.school_db.get_school(id)
            school['Reviews'][data['Username']] = data['Review']
            school['Ratings'].append(data['Rating'])
            self.school_db.update_json();
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def POST(self):
        output = {'result': 'success'}
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))

        try:
            id = len(list(self.school_db.get_schools()))
            self.school_db.schools[str(id)] = {
                'Name': data['Name'],
                'Enrollment': data['Enrollment'],
                'Graduation Rate': data['Graduation Rate'],
                'Price': data['Price'],
                'Url': data['Url'],
                'Image': data['Image'],
                'Reviews': {},
                'Ratings': []
            }
            output['id'] = str(id)
            self.school_db.update_json();
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def DELETE_KEY(self, id):
        output = {'result': 'success'}
        try:
            self.school_db.delete_school(id)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def DELETE_ALL(self):
        output = {'result': 'success'}

        try:
            schools = list(self.school_db.get_schools())
            for id in schools:
                self.school_db.delete_school(id)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
