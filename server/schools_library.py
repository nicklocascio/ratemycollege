import json
import pprint

class _schools_database:
    def __init__(self):
        self.schools = dict()

    # loads schools from colleges.json into the schools dictionary
    def load_schools(self, file):
        with open(file) as f:
            data = json.load(f)
            for key, value in data.items():
                # print(key)
                val_dict = {}
                for subkey, subval in value.items():
                    # print(subkey)
                    # print(subval)
                    val_dict[subkey] = subval
                self.schools[key] = val_dict

    def update_json(self):
        new_json = json.dumps(self.schools)
        fp = open("colleges.json", "w")
        fp.write(new_json)
        fp.close()

    # returns all of the school ids
    def get_schools(self):
        # resp = []
        # for key, value in self.schools.items():
        #     resp.append(value)
        #
        # return resp
        return self.schools.keys()

    # returns the data for a school given an id
    def get_school(self, id):
        try:
            school = self.schools[id]
        except Exception as ex:
            school = None

        return school

    # TODO set_school(self, id, data)
    def set_school(self, id, data):
        success = 0
        if id in self.schools:
            for newkey in data.keys():
                if newkey in self.schools[id]:
                    self.schools[id][newkey] = data[newkey]
            success = 1
            self.update_json()

        return success

    # deletes a school given an id
    def delete_school(self, id):
        del(self.schools[id])

# just testing code
if __name__ == "__main__":
    school_db = _schools_database()
    school_db.load_schools('colleges.json')
    pprint.pprint(school_db.schools)

    schools = school_db.get_schools()
    for school in schools:
        print(school)

    # trying to get length
    print(len(schools))
