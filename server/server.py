import cherrypy
import routes
import json

from schoolsController import SchoolController
from resetController import ResetController
from schools_library import _schools_database

def start_service():
    # create a _schools_database object to pass into controllers
    school_db = _schools_database()

    # initialize controllers (schools will be loaded on creation)
    schoolController = SchoolController(school_db=school_db)
    resetController = ResetController(school_db=school_db)

    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # dispatchers
    dispatcher.connect('school_get', '/schools/',
    controller=schoolController, action='GET', conditions=dict(method=['GET']))

    dispatcher.connect('school_get_key', '/schools/:id',
    controller=schoolController, action='GET_KEY', conditions=dict(method=['GET']))

    dispatcher.connect('school_put_key', '/schools/:id',
    controller=schoolController, action='PUT_KEY', conditions=dict(method=['PUT']))

    dispatcher.connect('school_put_review', '/schools/:id/review',
    controller=schoolController, action='PUT_REVIEW', conditions=dict(method=['PUT']))

    dispatcher.connect('school_post', '/schools/',
    controller=schoolController, action='POST', conditions=dict(method=['POST']))

    dispatcher.connect('school_delete_key', '/schools/:id',
    controller=schoolController, action='DELETE_KEY', conditions=dict(method=['DELETE']))

    dispatcher.connect('school_delete_all', '/schools/',
    controller=schoolController, action='DELETE_ALL', conditions=dict(method=['DELETE']))

    # RESET controller used for unit testing
    dispatcher.connect('reset_all', '/reset/',
    controller=resetController, action='PUT_INDEX', conditions=dict(method=['PUT']))

    # OPTIONS handlers for CORS
    dispatcher.connect('school_options', '/schools/',
    controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('school_options', '/schools/:id',
    controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('school_options', '/schools/:id/review',
    controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    conf = {
        'global': {
            'server.socket_host': 'localhost',
            'server.socket_port': 51045
        },
        '/': {
            'request.dispatch': dispatcher,
            'tools.CORS.on': True,
        }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)

    cherrypy.quickstart(app)

# options controller class
class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

# function for CORS
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
