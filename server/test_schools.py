import unittest
import requests
import json
from schools_library import _schools_database

class TestSchoolsIndex(unittest.TestCase):
    SITE_URL = 'http://localhost:51045'
    SCHOOL_URL = SITE_URL + '/schools/'
    RESET_URL = SITE_URL + '/reset/'
    print("testing for server: " + SITE_URL)
    school_db = _schools_database()
    school_db.load_schools('colleges.json')

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data=json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    # TODO test for GET_ALL
    def test_schools_get_all(self):
        self.reset_data()
        r = requests.get(self.SCHOOL_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['0']['Name'], 'University of Notre Dame')
        self.assertEqual(resp['44']['Name'], 'Loyola University Chicago')

    # test for POST
    def test_schools_post(self):
        self.reset_data()
        id = list(self.school_db.get_schools())[-1]
        school = {}
        school['Name'] = 'Queens University'
        school['Enrollment'] = 5000
        school['Graduation Rate'] = '95%'
        school['Price'] = '$34,000'
        school['Url'] = 'queens.edu'
        school['Image'] = 'queens.img'
        r = requests.post(self.SCHOOL_URL, data=json.dumps(school))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['id'], str(int(id)+1))

        r = requests.get(self.SCHOOL_URL + resp['id'])
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        data = resp['school']
        self.assertEqual(data['Name'], school['Name'])
        self.assertEqual(data['Enrollment'], school['Enrollment'])
        self.assertEqual(data['Graduation Rate'], school['Graduation Rate'])
        self.assertEqual(data['Price'], school['Price'])
        self.assertEqual(data['Url'], school['Url'])
        self.assertEqual(data['Image'], school['Image'])

    # test for DELETE_ALL
    def test_schools_delete_all(self):
        self.reset_data()

        m = {}
        r = requests.delete(self.SCHOOL_URL, data=json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        # need GET_ALL to be defined for this to work
        r = requests.get(self.SCHOOL_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(len(resp.keys()), 1)

if __name__ == "__main__":
    unittest.main()
