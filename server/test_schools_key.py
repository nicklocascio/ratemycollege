import unittest
import requests
import json

class TestSchools(unittest.TestCase):
    SITE_URL = 'http://localhost:51045'
    SCHOOL_URL = SITE_URL + '/schools/'
    RESET_URL = SITE_URL + '/reset/'
    print("testing for server: " + SITE_URL)

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data=json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    # test for GET_KEY
    def test_schools_get_key(self):
        self.reset_data()
        school_id = 17
        r = requests.get(self.SCHOOL_URL + str(school_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        school = resp['school']
        self.assertEqual(school['Name'], 'Michigan State University')
        self.assertEqual(school['Enrollment'], 49809)
        self.assertEqual(school['Graduation Rate'], '81%')
        self.assertEqual(school['Price'], '$16,579')
        self.assertEqual(school['Url'], 'https://msu.edu/')
        self.assertEqual(school['Image'], 'https://i.pinimg.com/originals/d9/8e/bc/d98ebce767c1893fe259a3f783cb4f59.jpg')

    # test for PUT_KEY
    def test_schools_put_key(self):
        self.reset_data()
        school_id = 0
        body = {'Enrollment': 30000}
        r = requests.put(self.SCHOOL_URL + str(school_id), data=json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.SCHOOL_URL + str(school_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['school']['Enrollment'], body['Enrollment'])
        body = {'Enrollment': 12683}
        r = requests.put(self.SCHOOL_URL + str(school_id), data=json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.SCHOOL_URL + str(school_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['school']['Enrollment'], body['Enrollment'])

    # test for PUT_REVIEW
    def test_schools_put_review(self):
        self.reset_data()
        school_id = 0
        body = {
            'Username': 'nicklocascio',
            'Review': 'Awesome school with great professors',
            'Rating': 5}
        r = requests.put(self.SCHOOL_URL + str(school_id) + '/review', data=json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.SCHOOL_URL + str(school_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['school']['Reviews'][body['Username']], body['Review'])
        self.assertEqual(resp['school']['Ratings'][0], body['Rating'])

    # test for DELETE_KEY
    def test_schools_delete_key(self):
        self.reset_data()
        school_id = 0
        body = {}
        r = requests.delete(self.SCHOOL_URL + str(school_id), data=json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.SCHOOL_URL + str(school_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['school'], None)

if __name__ == "__main__":
    unittest.main()
